const usuarios = [
  {
    id: 1,
    nombre: "Pablo",
    apellido: "Bartolomé",
  },
  {
    id: 2,
    nombre: "Rosalía",
    apellido: "Suarez",
  },
  {
    id: 3,
    nombre: "Priscila",
    apellido: "Castellanos",
  },
  {
    id: 4,
    nombre: "Elias",
    apellido: "Gallego",
  },
  {
    id: 5,
    nombre: "Miguel",
    apellido: "Canals",
  },
  {
    id: 6,
    nombre: "Camila",
    apellido: "Zamorano",
  },
  {
    id: 7,
    nombre: "Julia",
    apellido: "Neira",
  },
  {
    id: 8,
    nombre: "Óscar",
    apellido: "Valverde",
  },
];

export default usuarios;
