import React from "react";

const MatrixTable = ({ matrix }) => {
  return (
    <div>
      <table>
        <tbody>
          {matrix.map((row, index) => (
            <tr key={index}>
              {row.map((col, index) => (
                <td key={index}>{col}</td>
              ))}
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default MatrixTable;
