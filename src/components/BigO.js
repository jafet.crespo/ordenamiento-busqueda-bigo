import React from "react";

const BigO = () => {
  return (
    <div>
      <h2>Grafica BIG O</h2>
      <img
        id="bigO-img"
        src="https://danielmiessler.com/images/big-o-chart-tutorial-bazar-aymptotic-notations-1.png"
        alt="big O"
      />
    </div>
  );
};

export default BigO;
