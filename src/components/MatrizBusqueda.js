import React, { useState } from "react";
import busquedaBinaria from "../Helpers/BusquedaBinaria";
import MatrixTable from "./MatrixTable";

const matrix = [
  [1, 3, 6, 8],
  [11, 13, 16, 19],
  [20, 22, 25, 28],
];

const MatrizBusqueda = () => {
  const [busqueda, setBusqueda] = useState(0);
  const [textoBusqueda, setTextoBusqueda] = useState(
    "Ingrese un número a buscar!!"
  );

  const handleSearch = (e) => {
    setBusqueda(e.target.value);
  };

  const realizarBusqueda = () => {
    const resp = busquedaBinaria(matrix, parseInt(busqueda));
    setTextoBusqueda(resp);
  };

  return (
    <div>
      <h2>METODO DE BUSQUEDA BINARIO</h2>
      <p>
        La búsqueda binaria es un algoritmo de búsqueda que se utiliza en
        estructuras de datos ordenadas, como una lista o un array. Funciona
        dividiendo repetidamente la estructura de datos en dos partes hasta que
        se encuentra el elemento deseado o se determina que no está presente. En
        cada iteración, se comparan el valor del elemento en el medio con el
        valor deseado y se descartan la mitad de los elementos restantes según
        sea necesario.
      </p>
      <input
        type="number"
        id="busqueda"
        value={busqueda}
        onChange={handleSearch}
      />
      <button onClick={realizarBusqueda}>BUSCAR</button>
      <p>{textoBusqueda}</p>
      <MatrixTable matrix={matrix} />
    </div>
  );
};

export default MatrizBusqueda;
