import React from "react";
import BigO from "./BigO";
import Burbuja from "./Burbuja";
import MatrizBusqueda from "./MatrizBusqueda";

const Home = () => {
  return (
    <div id="home">
      <Burbuja />
      <MatrizBusqueda />
      <BigO />
    </div>
  );
};

export default Home;
