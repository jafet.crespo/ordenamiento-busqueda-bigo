import React, { useState } from "react";
import usuarios from "../DB/UsuariosDB";
import ordenarBurbuja from "../Helpers/OrdenarBurbuja";
import UsersTable from "./UsersTable";

const Burbuja = () => {
  const [opcion, setOpcion] = useState("id");

  const handleChange = (e) => {
    setOpcion(e.target.value);
  };

  return (
    <div>
      <h2>METODO DE ORDENAMIENTO BURBUJA</h2>
      <p>
        El método de la burbuja es un algoritmo de ordenamiento simple que se
        utiliza para ordenar una lista de elementos. Este método es sencillo de
        implementar y fácil de entender, pero es muy ineficiente en términos de
        tiempo de ejecución, especialmente para listas grandes.
      </p>
      <img
        src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/c8/Bubble-sort-example-300px.gif/250px-Bubble-sort-example-300px.gif"
        alt="burbuja"
      />
      <p>
        Se le llama método burbuja debido a la forma en que los valores más
        grandes "burbujean" hasta el final del array en cada iteración. El
        algoritmo funciona comparando dos elementos consecutivos y
        intercambiándolos si están en el orden incorrecto. Esto hace que los
        valores más grandes se muevan hacia la parte final del array, como
        burbujas subiendo a la superficie.
      </p>
      <p id="order-text">Ordenar por: </p>
      <div id="inputs-container">
        <input
          type="radio"
          value="id"
          id="id"
          name="opcion"
          onChange={handleChange}
        />
        <label htmlFor="id">Id</label>
        <input
          type="radio"
          value="nombre"
          id="nombre"
          name="opcion"
          onChange={handleChange}
        />
        <label htmlFor="nombre">Nombre</label>
        <input
          type="radio"
          value="apellido"
          id="apellido"
          name="opcion"
          onChange={handleChange}
        />
        <label htmlFor="apellido">Apellido</label>
      </div>
      <UsersTable usuarios={ordenarBurbuja(usuarios, opcion)} />
    </div>
  );
};

export default Burbuja;
