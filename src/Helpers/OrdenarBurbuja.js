export default function ordenarBurbuja(lista, atributo = "nombre") {
  let listaCopy = [...lista];
  let conCambios = false;
  let iteracion = 0;

  do {
    conCambios = false;
    for (let i = 0; i < listaCopy.length - 1 - iteracion; i++) {
      if (listaCopy[i][atributo] > listaCopy[i + 1][atributo]) {
        let temp = listaCopy[i];
        listaCopy[i] = listaCopy[i + 1];
        listaCopy[i + 1] = temp;
        conCambios = true;
      }
    }
    iteracion++;
  } while (conCambios === true);

  return listaCopy;
}
