export default function busquedaBinaria(matrix, busqueda) {
  const rowLength = matrix.length;
  const colLength = matrix[0].length;
  let low = 0;
  let high = rowLength * colLength;

  while (low < high) {
    let mid = Math.floor((high + low) / 2);

    if (matrix[Math.floor(mid / colLength)][mid % colLength] === busqueda)
      return (
        "elemento encontrado en la pos: " +
        [Math.floor(mid / colLength), mid % colLength]
      );
    else if (matrix[Math.floor(mid / colLength)][mid % colLength] < busqueda)
      low = mid + 1;
    else high = mid;
  }
  return "no se encontro el número";
}
