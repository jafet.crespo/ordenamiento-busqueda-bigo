// Crear un grafo

//SOLO FUNCIONA EN NODE 16!!!

import { Dijkstra, Graph } from "js-graph-algorithms";

/* crea 5 nodos, cada nodo es representado por un valor númerico empezando en 0
  hasta n - 1 donde n es el número total de nodos
*/
let graph = new Graph(2);

/*addEdge agrega una arista a un grafo. el primer parametro es el nodo de inicio,
el segundo es el nodo de destino y el tercer parametro el peso de la arista.
*/

graph.addEdge(0, 1, 4);
graph.addEdge(0, 2, 2);
graph.addEdge(2, 1, 1);
graph.addEdge(1, 3, 2);
graph.addEdge(2, 3, 5);
graph.addEdge(3, 4, 3);
graph.addEdge(4, 4, 6);

// Ejecutar el algoritmo de Dijkstra desde el nodo 0 hasta el nodo 4
export const distanciaCorta = Dijkstra(graph, 0, 4);

console.log(distanciaCorta);
